
import React from 'react';
import Client from './Client';
import Moment from 'moment';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const MATCHING_ITEM_LIMIT = 50;
const favIconSearchURL = 'https://www.google.com/s2/favicons?domain='


class TrafficList extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    trafficArr: [],
  };

  componentDidMount() {
    const value = '';

    Client.getAllTraffic(value, (trafficArr) => {
      this.setState({
        trafficArr: trafficArr.slice(0, MATCHING_ITEM_LIMIT),
      });
    });
   }


 handleChange(date) {
   var dateUx = date.unix() * 1000 + 68400000;
   //var dateUx = Moment( date ).valueOf()
   this.setState({
     startDate: date
   });

   Client.getTrafficByDate(dateUx, (trafficArr) => {
     console.log(trafficArr);
     trafficArr.sort(function(a,b) {return (a.visits > b.visits) ? 1 : ((b.visits > a.visits) ? -1 : 0);} ).reverse();
     this.setState({
       trafficArr: trafficArr.slice(0, 5),
     });
   });
 };

  render() {
    const { trafficArr } = this.state;
    const trafficRows = trafficArr.map((traffic, idx) => (
      <tr key={idx}>
        <td><img  className='favClass' src={favIconSearchURL + traffic.website}/>{traffic.website}</td>
        <td className='left aligned'>{Moment(traffic.date).format("MMM-DD-YYYY")}</td>
        <td className='right aligned'>{traffic.visits}</td>
      </tr>
    ));

    const datesWithTraffic = [...new Set(trafficArr.map(item => Moment(item.date).format("MM/DD/YYYY")))];

    return (
      <div id='food-search'>
        <table className='ui selectable structured large table'>
          <thead>
            <tr>
              <th colSpan='3'>
                <div className='ui fluid search'>
                  <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    highlightDates={datesWithTraffic}
                    placeholderText="Select date" />
                </div>
              </th>
            </tr>
            <tr>
              <th className='wide'>Website</th>
              <th>Date</th>
              <th className= "right aligned">Visits</th>
            </tr>
          </thead>
          <tbody>
            {trafficRows}
          </tbody>
        </table>
      </div>
    );
  }
}

export default TrafficList;
