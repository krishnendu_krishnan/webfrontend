/* eslint-disable no-undef */
function getAllTraffic(query, cb) {
  return fetch(`http://192.168.33.10:8080/getAllTraffic`, {
    accept: 'application/json',
  }).then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

function getTrafficBySite(query, cb) {
  return fetch(`http://54.243.8.123:8080/getChartData`, {
    accept: 'application/json',
  }).then(checkStatus)
    .then(parseJSON)
    .then(cb);
}


function getTrafficByDate(query, cb) {
  return fetch(`http://54.243.8.123:8080/getTrafficByDate?dateLong=${query}`, {
    accept: 'application/json',
  }).then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

function getVisitsByDate(query, cb) {
  return fetch(`http://54.243.8.123:8080/getVisitsByDate`, {
    accept: 'application/json',
  }).then(checkStatus)
    .then(parseJSON)
    .then(cb);
}
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error); // eslint-disable-line no-console
  throw error;
}

function parseJSON(response) {
  return response.json();
}

const Client = { getAllTraffic, getTrafficBySite, getVisitsByDate , getTrafficByDate};
export default Client;
