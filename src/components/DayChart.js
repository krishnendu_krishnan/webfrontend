import React, { Component } from 'react';


import "react-vis/main.css";
import {XYPlot, XAxis, YAxis, VerticalBarSeries} from 'react-vis';
import Client from '../Client';


const MATCHING_ITEM_LIMIT = 5;
class DayChart extends React.Component {
    constructor(props) {
      super(props);
      // this.handleDayClick = this.handleDayClick.bind(this);
      // this.handleChange = this.handleChange.bind(this);
    }
    state = {
      trafficArr: [],
      selectedDay: null,
    };

    componentDidMount() {
      console.log("Component did mount");
      const value = '';

      Client.getTrafficBySite(value, (trafficArr) => {
        console.log(trafficArr);
        this.setState({
          trafficArr: trafficArr.slice(0, MATCHING_ITEM_LIMIT),
        });
      });
     }

    render(){
      const chartData = [];
      this.state.trafficArr.map(function(tr) {
        chartData.push({x: tr.website, y: tr.totalVisits });
      });

      return(
        <XYPlot
        margin={{bottom: 70, left: 100}}
          xType="ordinal"
          width={320}
          height={250}>

          <XAxis tickLabelAngle={-35} />
          <YAxis />

          <VerticalBarSeries
            className="vertical-bar-series"
            data={chartData}/>

        </XYPlot>
      )
    }
}

export default DayChart;
