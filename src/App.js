import React, { Component } from 'react';
import TrafficList from './TrafficList';
import DayChart from './components/DayChart';
//import TrafficLine from './components/TrafficLine';

import "react-vis/main.css";

class App extends Component {


  render() {
    return (
      <div className='App'>
        <div className="ui internally celled grid">
          <h1 className="ui header">Web Traffic Analytics</h1>

          <div className="row">
            <div className="ten wide column">
            <TrafficList />
            </div>
            <div className="three wide column">
            <h4 className="ui header">Top 5 websites by visits</h4>
            <DayChart />

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
