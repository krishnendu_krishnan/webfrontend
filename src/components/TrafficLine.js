import React, { Component } from 'react';

import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import "react-vis/main.css";
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries
} from 'react-vis';
import Client from '../Client';


const MATCHING_ITEM_LIMIT = 5;
class TrafficLine extends React.Component {
    constructor(props) {
      super(props);
      // this.handleDayClick = this.handleDayClick.bind(this);
      // this.handleChange = this.handleChange.bind(this);
    }
    state = {
      trafficArr: [],
      selectedDay: null,
    };

    componentDidMount() {
      console.log("Component did mount");
      const value = '';

      Client.getVisitsByDate(value, (trafficArr) => {
        console.log(trafficArr);
        this.setState({
          trafficArr: trafficArr.slice(0, MATCHING_ITEM_LIMIT),
        });
      });
     }

    render(){
      const chartData = [];
      this.state.trafficArr.map(function(tr) {
        chartData.push({x: tr.visitDate, y: tr.totalVisits });
      });

      return(
        <XYPlot
        margin={{bottom: 70, left: 100}}
        xType="time"
        width={300}
        height={300}>
        <HorizontalGridLines />
        <VerticalGridLines />
        <XAxis title="Time" />
        <YAxis title="Total Visits" />
        <LineSeries
          data={chartData}/>
        <LineSeries
          data={null}/>

      </XYPlot>
      )
    }
}

export default TrafficLine;
