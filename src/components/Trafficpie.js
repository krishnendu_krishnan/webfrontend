import React, { Component } from 'react';

import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import "react-vis/main.css";
import {XYPlot, XAxis, YAxis, VerticalGridLines, HorizontalGridLines, LineSeries, VerticalBarSeries} from 'react-vis';

import {RadialChart} from 'react-vis'
;

class TrafficPie extends React.Component {
    constructor(props) {
      super(props);
      // this.handleDayClick = this.handleDayClick.bind(this);
      // this.handleChange = this.handleChange.bind(this);
    }



    render(){
      return(
        <RadialChart
          showLabels
          innerRadius={100}
          radius={140}
            data={[
              {angle: 2},
              {angle: 6},
              {angle: 2},
              {angle: 3},
              {angle: 1}
            ]}
        width={300}
        height={300}/>
      )
    }
}

export default TrafficPie;
